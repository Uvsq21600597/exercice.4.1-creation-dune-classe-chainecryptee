package EncryptedString;

public class EncryptedString {
	
		private int shift;
		private String  stored;
		
		
	private EncryptedString(int shift, String stored) {
			
		
	if(stored!=null) {
		this.stored = stored.toUpperCase();
	}
	else this.stored="";
	this.shift=shift;
	}
	
		
	
	private static char ChiftChar(char in_aChar, int in_aShift){
		return ( in_aChar < 'A' || in_aChar > 'Z' ) ? in_aChar : (char)(((in_aChar+in_aShift+'A') % 26) + 'A' );
	}
	
	
		
	public String encrypt(){
		String chaine = "" ;
		
		for (int i = 0; i < stored.length(); i++) {

			chaine = chaine + ChiftChar( stored.charAt(i) , shift );
			
		}
		return chaine;
	}
	
	public String decrypt(){
		
		return stored;
		
	}
	
	
	public static EncryptedString FromCrypted(String in_stored , int in_shift) {
		return  new EncryptedString(in_shift, in_stored);
	}
	
	public static EncryptedString FromDecrypted(String in_stored , int in_shift) {
		EncryptedString es = new EncryptedString(in_shift ,in_stored);
	
		return  new EncryptedString(in_shift, es.encrypt());
	}



	@Override
	public String toString() {
		return "EncryptedString [shift=" + shift + ", stored=" + stored + "]";
	}
	
	
		

}
